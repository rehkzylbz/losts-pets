<?php
	header('Content-Type: application/json; charset=utf-8');
	spl_autoload_register(function ($class_name) {
        include_once $_SERVER['DOCUMENT_ROOT'].'/app/classes/'.$class_name.'.php';
    });
	$result = [
		'status' => false, 
		'content' => ''
	];
	$function = substr($_SERVER['PATH_INFO'], 1);
	parse_str($_SERVER['QUERY_STRING'], $params);	
	if ( Api::check_auth($params) ) {	
		if ( method_exists('Api', $function) ) {
			$result = Api::$function($params);
		}
		else $result['content'] = 'Указана недоступная функция';		
	}
	else $result['content'] = 'Авторизация не пройдена';	
	echo json_encode($result, JSON_UNESCAPED_UNICODE);