<?php 
	foreach ($result as $pet) { 
		$type_options = '';
		foreach ($types as $key => $type) {
			$type_options .= '<option value="'.$key.'"';
			$type_options .= $key===(int)$pet['type_id'] ? ' selected' : '';
			$type_options .= '>'.$type['type_name'].'</option>';
		}
		$color_options = '';
		foreach ($colors as $key => $color) {
			$color_options .= '<option value="'.$key.'"';
			$color_options .= $key===(int)$pet['color_id'] ? ' selected' : '';
			$color_options .= '>'.$color['color_name'].'</option>';
		}
		$result_array[] = [
			'<input type="hidden" name="id" value="'.$pet['id'].'">'.$pet['id'],
			'<select name="type_id"  class="form-control">'.$type_options.'</select>',
			'<select name="color_id"  class="form-control">'.$color_options.'</select>',
			'<input type="number" class="form-control" name="weight" min="0" max="1000" step="1" value="'.$pet['weight'].'">',
			'<input type="number" class="form-control" name="height" min="0" max="200" step="1" value="'.$pet['height'].'">',
			'<button class="btn btn-primary mx-2 edit" title="Изменить">&#10004;</button>
			<button class="btn btn-danger mx-2 remove" title="Удалить">&#10008;</button>'
		];
	}