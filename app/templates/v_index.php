<!doctype html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="app/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="app/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="app/css/jquery.dataTables.css">
    <title>Потеряшки</title>
  </head>
  <body>  
	  <div class="container-fluid px-2 pb-5">
		<div class="col-12 row justify-content-center my-4">
			<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#lost" aria-expanded="true" title="Просмотреть список разыскиваемых">Разыскиваются</button>
		</div>
		<div class="collapse mb-5" id="lost">
			<table id="losts" class="table table-striped table-bordered table-hover">
				<thead class="thead-dark">
					<tr>
						<th class="sorting">#</th>
						<th class="sorting">Вид</th>
						<th class="sorting">Цвет</th>
						<th class="sorting">Вес</th>
						<th class="sorting">Рост</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($losts_pets as $key => $pet) { ?>
					<tr>
						<td class="text-center align-middle">
							<?=$key?>
						</td>
						<td class="text-center align-middle">
							<?=$pet['type_name']?>
						</td>
						<td class="text-center align-middle">
							<?=$pet['color_name']?>
						</td>
						<td class="text-center align-middle">
							<?=$pet['weight']?>
						</td>
						<td class="text-center align-middle">
							<?=$pet['height']?>
						</td>
					</tr>
				<?php } ?>
				</tbody>    
			</table>
		</div>		
		
			<h3 class="h3 text-center my-3">Поступил новичок:</h3>
		  <div class="col-lg-10 mx-auto my-3 mb-5 row justify-content-around" id="pet-form">
			<div class="form-group col-md-2">
				<label for="type_id">Вид</label>
					<select name="type_id" class="form-control">
						<?php foreach ($types as $key => $type) { ?>
							<option value="<?=$key?>"><?=$type['type_name']?></option>
						<?php } ?>
					</select>
			</div>
			<div class="form-group col-md-2">
				<label for="color_id">Цвет</label>
					<select name="color_id" class="form-control">
						<?php foreach ($colors as $key => $color) { ?>
							<option value="<?=$key?>"><?=$color['color_name']?></option>
						<?php } ?>
					</select>
			</div>
			<div class="form-group col-md-2">
				<label for="weight">Вес</label>
				<input type="number"  class="form-control" name="weight" min="0" max="1000" step="1" value="0">
			</div>
			<div class="form-group col-md-2">
				<label for="height">Рост</label>
				<input type="number"  class="form-control" name="height" min="0" max="200" step="1" value="0">
			</div>
			<div class="form-group col-md-2">
				<label for="add">&nbsp;</label>
				<button id="add" class="btn btn-primary d-block" name="add" title="Добавить">Добавить</button>
			</div>
			<form id="file_form" name="file_form">
				<div class="custom-file w-auto">
					<input type="hidden" name="send_file" id="send_file" value="">
					<input type="file" name="file" class="custom-file-input" id="file">
					<label class="custom-file-label" for="file">Выбрать файл</label>					
				</div>
			<form>
		</div>	
		
		<p id="result_box" class="" style="text-align: center;"></p>
							
		<h1 class="h1 text-center my-3">Наши постояльцы:</h1>
		<table id="pets" class="table table-striped table-bordered table-hover mb-5">
			<thead class="thead-dark">
				<tr>
					<th class="sorting">#</th>
					<th class="sorting">Вид</th>
					<th class="sorting">Цвет</th>
					<th class="sorting">Вес</th>
					<th class="sorting">Рост</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			</tbody>    
		</table>
		<div id="info-box" style="display: none;"></div>
	</div>
	
    <script src="app/js/jquery-3.5.1.min.js"></script>
    <script src="app/js/popper.min.js"></script>
    <script src="app/js/bootstrap.min.js"></script>
	<script src="app/js/bs-custom-file-input.min.js"></script>
    <script src="app/js/jquery-ui.min.js"></script>
    <script src="app/js/jquery.dataTables.js"></script>
    <script src="app/js/front.js"></script>
</body>
</html>