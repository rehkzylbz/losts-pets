<?php

class Pet {
	
	private $db;
    
    public function __construct() {
		try {    
			$this->db = new PDO('sqlite:'.$_SERVER['DOCUMENT_ROOT'].'/app/sqliteDBs/pets.db');
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db->exec( 'PRAGMA foreign_keys = ON;' );
		} catch (PDOException $e){
			 die ('Ошибка соединения с БД: '.$e->getMessage());
		}
    }
	
	public function get_types() {
        $query = $this->db->query('SELECT id, type_name FROM types WHERE status = 1');
		$query->execute();
		$result = $query->fetchAll(PDO::FETCH_UNIQUE);
        return $result;
    }
	
	public function get_colors() {
		$query = $this->db->query('SELECT id, color_name FROM colors WHERE status = 1');
		$query->execute();
		$result = $query->fetchAll(PDO::FETCH_UNIQUE);
        return $result;
    }
	
	public function get_losts_all() {
		$query = $this->db->query('SELECT lost_pets.id as id, lost_pets.weight as weight, lost_pets.height as height, types.type_name as type_name, colors.color_name as color_name FROM lost_pets LEFT JOIN types ON lost_pets.type_id = types.id LEFT JOIN colors ON lost_pets.color_id = colors.id WHERE  lost_pets.status = 1');
		$query->execute();
		$result = $query->fetchAll(PDO::FETCH_UNIQUE);
        return $result;
    }

    public function get_our_all() {
		$query = $this->db->query('SELECT * FROM our_pets WHERE status = 1');
		$query->execute();
		$result = $query->fetchAll();
		$types = $this->get_types();
		$colors = $this->get_colors();
		$result_array = [];
		include_once 'app/templates/v_get_our_all_arr.php';
        return $result_array;
    }
	
	public function add_new($data = [], $with_search = false) {
		$result = [
			'status' => false,
			'content' => 'Запрос к БД неудачен'
		];
		try {
			$query = $this->db->prepare("INSERT INTO our_pets (type_id, color_id, weight, height) VALUES (:type_id, :color_id, :weight, :height)");
			if ( self::check_dataset($data, ['type_id', 'color_id', 'weight', 'height']) ) {	
				$query->bindValue(':type_id', $data['type_id'], PDO::PARAM_INT);
				$query->bindValue(':color_id', $data['color_id'], PDO::PARAM_INT);
				$query->bindValue(':weight', $data['weight'], PDO::PARAM_INT);
				$query->bindValue(':height', $data['height'], PDO::PARAM_INT);
				if ( $query->execute() && $id = $this->db->lastInsertId() ) {			
					$result['status'] = true;
					$result['content'] = 'Запись произведена, идентификатор: '.$id;
					if ( $with_search ) $result['search'] = $this->search($id);
				}
			}
			else {
				$result['content'] = 'Отсутствуют необходимые параметры';
				return $result;
			}
		}
		catch (PDOException $e) {
			$result['content'] = 'Выполнение запроса не удалось, проверьте переданные параметры. ';
			return $result;
		}	
        return $result;
    }
	
	public function edit($data = []) {
        $result = [
			'status' => false,
			'content' => 'Запрос к БД неудачен',
			'search' => false
		];
		try {
			$query = $this->db->prepare("UPDATE our_pets SET type_id = :type_id, color_id = :color_id, weight = :weight, height = :height WHERE id = :id");
			if ( self::check_dataset($data, ['id', 'type_id', 'color_id', 'weight', 'height']) ) {	
				$query->bindValue(':id', $data['id'], PDO::PARAM_INT);		
				$query->bindValue(':type_id', $data['type_id'], PDO::PARAM_INT);
				$query->bindValue(':color_id', $data['color_id'], PDO::PARAM_INT);
				$query->bindValue(':weight', $data['weight'], PDO::PARAM_INT);
				$query->bindValue(':height', $data['height'], PDO::PARAM_INT);
				if ( $query->execute() ) {			
					$result['status'] = true;
					$result['content'] = 'Запись произведена';
					$result['search'] = $this->search($data['id']);
				}
			}
			else {
				$result['content'] = 'Отсутствуют необходимые параметры';
				return $result;
			}
		}
		catch (PDOException $e) {
			$result['content'] = 'Выполнение запроса не удалось, проверьте переданные параметры. ';
			return $result;
		}	
        return $result;
    }
	
	public function remove($data = []) {
        $result = [
			'status' => false,
			'content' => 'Запрос к БД неудачен',
		];
		try {
			$query = $this->db->prepare("UPDATE our_pets SET status = 0 WHERE id = :id");				
			if ( self::check_dataset($data, ['id']) ) {	
				$query->bindValue(':id', $data['id'], PDO::PARAM_INT);		
				if ( $query->execute() ) {			
					$result['status'] = true;
					$result['content'] = 'Запись произведена';
				}
			}
			else {
				$result['content'] = 'Отсутствуют необходимые параметры';
				return $result;
			}
		}
		catch (PDOException $e) {
			$result['content'] = 'Выполнение запроса не удалось, проверьте переданные параметры. ';
			return $result;
		}	
        return $result;
    }
	
	private function search($id = 0) {  
		$result = [
			'status' => false,
			'content' => 'Запрос к БД неудачен'
		];	
		try {
			$query = $this->db->prepare("SELECT * FROM our_pets WHERE id = :id");
			$query->bindValue(':id', $id, PDO::PARAM_INT);
			if ( $query->execute() && $new_pet = $query->fetch() ) {
				try {
					$query = $this->db->prepare("SELECT * FROM lost_pets LEFT JOIN types ON lost_pets.type_id = types.id LEFT JOIN colors ON lost_pets.color_id = colors.id WHERE type_id = :type_id AND color_id = :color_id AND weight = :weight AND height = :height AND lost_pets.status = 1");
					$query->bindValue(':type_id', $new_pet['type_id'], PDO::PARAM_INT);
					$query->bindValue(':color_id', $new_pet['color_id'], PDO::PARAM_INT);
					$query->bindValue(':weight', $new_pet['weight'], PDO::PARAM_INT);
					$query->bindValue(':height', $new_pet['height'], PDO::PARAM_INT);
					if ( $query->execute() ) {
						$rows = [];
						while ( $lost_pet = $query->fetch() ) {
							$rows[] = $lost_pet;
						}
						if ( count($rows) ) {
							$result['status'] = true;
							$result['content'] = $rows;
						}
						else $result['content'] = 'Соответствий не найдено';
					}
				}
				catch (PDOException $e) {
					$result['content'] = $e->getMessage();
					return $result;
				}
			}
		}
		catch (PDOException $e) {
			$result['content'] = $e->getMessage();
			return $result;
		}	
		return $result;
    }
	
	private static function check_dataset($data = [], $fields = []) {  
		$result = true;	
		foreach ( $fields as $field ) {
			if ( !isset($data[$field]) ) {
				$result = false;
				break;
			}
		}
		return $result;
    }
    
}