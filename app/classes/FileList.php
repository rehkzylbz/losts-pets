<?php

class FileList {
	
	private static $files_dir = 'app/files/';
	private static $allow_ext = ['csv'];
	private static $max_file_size = 10240;
	private static $minimum_data_fields = 4;
    
    public static function process($files = []) {
		$result = self::load_file($files);
		if ( $result['status'] ) $result = self::handle_file($result['content']);
		return $result;
    }
	
	private static function load_file($files = []) {
		$result = [
			'status' => false,
			'content' => 'Загрузка файла неудачна'
		];	
		if ( empty($files) ) {		
			$result['content'] = 'Файл не выбран.';
			return $result;
		}	
		if ( !is_uploaded_file($files['file']['tmp_name']) ) {    
			$result['content'] = 'Не удалось загрузить файл.';
			return $result;
		}			
		if ( filesize($files['file']['tmp_name']) > self::$max_file_size ) {
			$result['content'] = 'Недопустимый размер файла.';
			return $result;
		}	
		if ( !in_array(strtolower(pathinfo($files['file']['name'], PATHINFO_EXTENSION)), self::$allow_ext) ) {	
			$result['content'] = 'Недопустимый тип файла.';
			return $result;
		}	
		if( !is_dir(self::$files_dir) ) mkdir(self::$files_dir);
		$filename = self::$files_dir.time().'_'.$files['file']['name'];
		if( !move_uploaded_file($files['file']['tmp_name'], $filename) ) {	
			$result['content'] = 'Не удалось сохранить файл.';
			return $result;		
		} 				
		$result['status'] = true;
		$result['content'] = realpath($filename);			
        return $result;
    }
	
	private static function handle_file($filename = '') {
		$result = [
			'status' => false,
			'content' => 'Обработка файла неудачна'
		];				
		if ( ($handle = fopen($filename, 'r')) !== false ) {
			$pet = new Pet();
			$i = 0;
			while ( ($data = fgetcsv($handle, 1000, ',')) !== false ) {
				if ( count($data) >= self::$minimum_data_fields ) {
					$params = [
						'type_id' => $data[0],
						'color_id' => $data[1],
						'weight' => $data[2],
						'height' => $data[3],
					];	
					$new_pet = $pet->add_new($params, true);
					if ( $new_pet['status'] ) {
						$i++;
						if ( $new_pet['search']['status'] ) {
							$result['search']['status'] = true;
							foreach ( $new_pet['search']['content'] as $finded ) {
								$result['search']['content'][] = $finded;	
							}
						}
					}
				}
			}
			$result['status'] = true;
			$result['content'] = 'Файл обработан, количество добавленных строк: '.$i;
			fclose($handle);
		}
		else {
			$result['content'] = 'Открыть файл для чтения не удалось.';
			return $result;
		}
        return $result;
    }
	    
}