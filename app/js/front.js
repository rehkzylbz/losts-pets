$(document).ready(function() {
	bsCustomFileInput.init();
	let success_text_class = 'text-success',
		error_text_class = 'text-danger',
		our_pets_table = $('#pets'),
		our_pets_datatable = our_pets_table.DataTable({
			"searching": true,
			"pageLength": 5,
			"lengthChange": false,
			"order": [[0, 'desc']],
			"columns": [  
			  { "orderable": true },
			  { "orderable": false },
			  { "orderable": false },
			  { "orderable": false },
			  { "orderable": false },
			  { "orderable": false },
			],
			'language': {
				'url': 'app/js/russian.json'
			}
		});
	$('#losts').DataTable({
		"searching": true,
		"pageLength": 5,
		"lengthChange": false,
		"order": [[0, 'asc']],
		'language': {
            'url': 'app/js/russian.json'
        }
	});    
	$("#info-box").dialog({
		autoOpen: false,
		width: 'auto',
		buttons: [
			{
			  text: "Печать",
			  click: function() {
				search_print(); 
			  }
			}
		  ]
	});
		
	function get_our_all() {
		console.time('get_our_all');
		$.ajax({
				type: "POST",
				url: "/",
				data: {
					get_our_all:true
				},            
				success: function(response){	
					result = JSON.parse(response);
					console.log('get_our_all success result', result);		
					our_pets_datatable.clear();	
					our_pets_datatable.rows.add(result);		
					our_pets_datatable.draw();
					console.timeEnd('get_our_all');
				},
				error: function(response) {
					console.log('get_our_all error response', response);
					console.timeEnd('get_our_all');
				}
		});
	};
	get_our_all();
	
	$('#add').on('click', function(){
		console.time('add');
		let row = $('#pet-form'),
			id = row.find('[name=id]').val(),
			type_id = row.find('[name=type_id]').val(),
			color_id = row.find('[name=color_id]').val(),
			weight = row.find('[name=weight]').val(),
			height = row.find('[name=height]').val(),
			result_box = $('#result_box');			
		result_box.hide();
		result_box.html('');
		$.ajax({
				type: "POST",
				url: "/",
				data: {
					id:id,
					type_id:type_id,
					color_id:color_id,
					weight:weight,
					height :height ,
					add:true
				},            
				success: function(response){
					result = JSON.parse(response);
					console.log('add success result', result);
					result_box.text(result.content);
					result_box.attr('class', result.status?success_text_class:error_text_class).show('slow');
					show_search(result.search);
					get_our_all();
					console.timeEnd('add');
				},
				error: function(response) {
					result = JSON.parse(response);
					console.log('add error result', result);
					result_box.html = 'Запрос к серверу неудачен';
					result_box.attr('class', error_text_class).show('slow');
					console.timeEnd('add');
				}
		});
	});
	
	$('body').on('click', '.edit', function() {
		console.time('edit');
		let row = $(this).parents('tr'),
			id = row.find('[name=id]').val(),
			type_id = row.find('[name=type_id]').val(),
			color_id = row.find('[name=color_id]').val(),
			weight = row.find('[name=weight]').val(),
			height = row.find('[name=height]').val(),
			result_box = $('#result_box');
		result_box.hide();
		result_box.html('');
		$.ajax({
				type: "POST",
				url: "/",
				data: {
					id:id,
					type_id:type_id,
					color_id:color_id,
					weight:weight,
					height :height ,
					edit:true
				},            
				success: function(response){
					result = JSON.parse(response);
					console.log('edit success result', result);
					result_box.text(result.content);
					result_box.attr('class', result.status?success_text_class:error_text_class).show('slow');
					show_search(result.search);
					console.timeEnd('edit');
				},
				error: function(response) {
					result = JSON.parse(response);
					console.log('edit error result', result);
					result_box.html = 'Запрос к серверу неудачен';
					result_box.attr('class', error_text_class).show('slow');
					console.timeEnd('edit');
				}
		});
	});
	
	function show_search(data = {}) {
		if ( data.status ) {
			let text = '<h4>Обнаружено в базе "Потеряшек":</h4><br><table class="table table-striped table-bordered table-hover mb-5">';
			for (row of data.content) {
				text += '<tr>'+'<td>'+row.type_name+'</td>'+'<td>цвет '+row.color_name+'</td>'+'<td>вес '+row.weight+'</td>'+'<td>рост '+row.height+'</td>'+'<td>'+row.owner_contacts+'</td></tr>'
			}
			text += '</table>'
			$('#info-box').html(text).dialog('open');
		}
	};
	
	function search_print() {
		let text = '<link rel="stylesheet" href="app/css/bootstrap.min.css">'+$('#info-box').html()+'<script>window.print();</script>',
			new_win = window.open('');
		new_win.document.write(text);  
	};
	
	$('body').on('click', '.remove', function() {
		console.time('remove');
		let row = $(this).parents('tr'),
			id = row.find('[name=id]').val(),
			result_box = $('#result_box');
		result_box.hide();
		result_box.html('');
		$.ajax({
				type: "POST",
				url: "/",
				data: {
					id:id,
					remove:true
				},            
				success: function(response){
					result = JSON.parse(response);
					console.log('remove success result', result);
					result_box.text(result.content);
					result_box.attr('class', result.status?success_text_class:error_text_class).show('slow');
					get_our_all();
					console.timeEnd('remove');
				},
				error: function(response) {
					result = JSON.parse(response);
					console.log('remove error result', result);
					result_box.html = 'Запрос к серверу неудачен';
					result_box.attr('class', error_text_class).show('slow');
					console.timeEnd('remove');
				}
		});
	});
	
	$('#file').on('change', function(){
		if ( this.value ) {
			console.log('Выбран файл!', this.value);
			send_file(this);
		} 
		else {
			console.log('Файл не выбран'); 
		}
	});
	
	function send_file(input) {
		console.time('send_file');
		let formData = new FormData(document.forms['file_form']),
			result_box = $('#result_box');
		result_box.hide();
		result_box.html('');
		console.log('send_file formData', formData);
		$.ajax({
				type: "POST",
				url: "/",
				data: formData, 
				dataType : 'json',
				processData : false,
				contentType : false,           
				success: function(result){
					console.log('send_file success result', result);
					result_box.text(result.content);
					result_box.attr('class', result.status?success_text_class:error_text_class).show('slow');
					show_search(result.search);
					get_our_all();
					input.value = '';
					console.timeEnd('send_file');
				},
				error: function(result) {
					console.log('send_file error response', result);
					result_box.html = 'Запрос к серверу неудачен';
					result_box.attr('class', error_text_class).show('slow');
					console.timeEnd('send_file');
				}
		});
	};
	
});