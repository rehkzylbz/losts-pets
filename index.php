<?php
	spl_autoload_register(function ($class_name) {
        include_once 'app/classes/'.$class_name.'.php';
    });
	$pet_object = new Pet();
	if (isset($_POST['get_our_all'])) {
			echo json_encode($pet_object->get_our_all());
			die;
	}
	if (isset($_POST['add'])) {
			echo json_encode($pet_object->add_new($_POST, true));
			die;
	}
	if (isset($_POST['edit'])) {
			echo json_encode($pet_object->edit($_POST));
			die;
	}
	if (isset($_POST['remove'])) {
			echo json_encode($pet_object->remove($_POST));
			die;
	}	
	if (isset($_POST['send_file'])) {
			echo json_encode(FileList::process($_FILES));
			die;
	}	
	$types = $pet_object->get_types();
	$colors = $pet_object->get_colors();
	$losts_pets = $pet_object->get_losts_all();		
	include_once 'app/templates/v_index.php';